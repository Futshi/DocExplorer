package DocExplorer;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

// PDF & LIRE Test
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageTree;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.graphics.PDXObject;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.rendering.PDFRenderer;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Arrays;

import net.semanticmetadata.lire.builders.*;
import net.semanticmetadata.lire.utils.*;
import net.semanticmetadata.lire.imageanalysis.features.global.EdgeHistogram;

import DocExplorer.PDF.PDFHandler;
import DocExplorer.Image.ImageHandler;

public class Main extends Application {
  @Override
  public void start(Stage primaryStage) throws Exception{
// region PDF Test
    /*
    File pdfFile = new File("/home/futshi/Documents/TU/Bachelorarbeit/DocExplorer/resources/pdf/Informatik_BA_2014.pdf");
    // test 1 (pdfbox stuff, iterate through all DocExplorer.pdf elements, check for images and save them)
    System.out.println("<<PDFBox>> TEST 1");
    try(PDDocument document = PDDocument.load(pdfFile)) {
      PDPageTree list = document.getPages();
      System.out.println("<<PDFBox>> found " + list.getCount() + " pages");
      int pageCount = 0;
      for(PDPage page : list) {
        PDResources pdResources = page.getResources();
        System.out.println("<<PDFBox>> resources on page " + ++pageCount + ": " + pdResources.getXObjectNames().toString());
        int imageCount = 0;
        for(COSName name : pdResources.getXObjectNames()) {
          PDXObject o = pdResources.getXObject(name);
          if(o instanceof PDImageXObject) {
            System.out.println("<<PDFBox>> found resource which is an image");
            PDImageXObject image = (PDImageXObject)o;
            String filename = "/home/futshi/Documents/TU/Bachelorarbeit/DocExplorer/resources/img/" + "extracted-image-" + ++imageCount + ".png";
            ImageIO.write(image.getImage(), "png", new File(filename));
          }
          else {
            System.out.println("<<PDFBox>> found resource which is NOT an image");
          }
      }
      }
    }
    catch (IOException e) {
        System.err.println("Exception while trying to create DocExplorer.pdf document - " + e);
    }
    // test 2 (pdfbox stuff, get first image of DocExplorer.pdf and save)
    System.out.println("<<PDFBOX>> TEST 2");
    PDDocument document = PDDocument.load(pdfFile);
    PDFRenderer pdfRenderer = new PDFRenderer(document);
    BufferedImage image = pdfRenderer.renderImage(0);
    ImageIO.write(image, "JPEG", new File("/home/futshi/Documents/TU/Bachelorarbeit/DocExplorer/resources/img/test2.jpeg"));
    System.out.println("<<PDFBox>> first page saved as image test2.jpeg");
    document.close();
    // test3 (extracting PDFs using custom class)
    /*
    System.out.println("<<PDFBox>> TEST 3");
    PDFHandler pdfHandler = new PDFHandler("/home/futshi/Documents/TU/Bachelorarbeit/DocExplorer/resources/pdf/");
    */
// endregion

// region LIRE Test
    /*
    File imgFolder = new File("/home/futshi/Documents/TU/Bachelorarbeit/DocExplorer/resources/img");
    System.out.println("<<LIRE>> TEST 4");
    if(imgFolder.isDirectory()) {
      System.out.println("<<LIRE>> image folder is directory");
      File[] imgFiles = imgFolder.listFiles(new FilenameFilter() {
        public boolean accept(File dir, String name) {
          return (name.endsWith(".png") || name.endsWith(".jpeg") || name.endsWith(".jpg"));
        }
      });
      System.out.println("<<LIRE>> " + imgFiles.length + " image file(s) found");
      // TODO use LIRE and extract some values
      EdgeHistogram edgeHistogram = new EdgeHistogram();
      for(int i = 0; i < imgFiles.length; i++) {
        edgeHistogram.extract(ImageIO.read(imgFiles[i]));
        System.out.println("<<LIRE>> imgFiles[" + i + "]: edge histogram feature vectors - " + Arrays.toString(edgeHistogram.getFeatureVector()));
      }
    }
    else {
      System.out.println("<<LIRE>> image folder is file (error)");
    }
    */
// endregion

    // *************************************************************************************************************
    // *************************************************************************************************************
    // *************************************************************************************************************

    // JavaFX (auto-generated)
    Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
    primaryStage.setTitle("DocExplorer");
    primaryStage.setScene(new Scene(root, 1000, 500));
    primaryStage.show();
  }

  /**
   * The main() method is ignored in correctly deployed JavaFX applications.
   * main() serves only as fallback in case the application can not be
   * launched through deployment artifacts, e.g., in IDEs with limited FX
   * support. NetBeans ignores main().
   * @param args
   */
  public static void main(String[] args) {
      launch(args);
  }
}
