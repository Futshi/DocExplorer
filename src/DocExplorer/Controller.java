package DocExplorer;

import DocExplorer.Image.ImageHandler;
import DocExplorer.PDF.PDFHandler;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.FileChooser;
import javafx.stage.DirectoryChooser;

import java.io.File;

// TODO JavaFX FileChooser

public class Controller {

  public Button fileChooserButton; // needs to match fxml component's ID
  public Button folderChooserButton; // needs to match fxml component's ID
  private FileChooser fileChooser;
  private File file;

  public void handleFileChooserButtonClick() {
    // just messing around
    fileChooserButton.setText("CLICKED FIRST");
    folderChooserButton.setText("NOT CLICKED FIRST");

    FileChooser fileChooser = new FileChooser();
    fileChooser.setTitle("ASDASDASD PDF FILE");
    fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("PDF Files", "*.pdf"));
    File file = fileChooser.showOpenDialog(fileChooserButton.getScene().getWindow());
    if(file != null) {
      System.out.println("<<FileChooser>> success (file chosen, not directory)");
      // TODO call PDFHandler?
      System.out.println("<<FileChooser>> choosing file does not do anything at the moment");
    }
    // TODO handle chosen stuff
  }

  public void handleFolderChooserButtonClick() {
    // just messing around
    fileChooserButton.setText("NOT CLICKED FIRST");
    folderChooserButton.setText("CLICKED FIRST");

    DirectoryChooser directoryChooser = new DirectoryChooser();
    directoryChooser.setTitle("ASDASDASD DIRECTORY");
    File file = directoryChooser.showDialog(folderChooserButton.getScene().getWindow());
    if(file != null) {
      System.out.println("<<FileChooser>> success (directory chosen, not file)");
      PDFHandler pdfHandler = new PDFHandler(file);
      // TODO at the moment hardcoded folder name because pdfs and extracted images are not in same folder
      ImageHandler imgHandler = new ImageHandler("/home/futshi/Documents/TU/Bachelorarbeit/DocExplorer/resources/img");
    }
    // TODO handle chosen stuff
  }
}
