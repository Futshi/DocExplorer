package DocExplorer.Image;

import net.semanticmetadata.lire.builders.SimpleDocumentBuilder;
import net.semanticmetadata.lire.imageanalysis.features.global.EdgeHistogram;
import javax.imageio.ImageIO;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Arrays;

public class ImageHandler {
    private String sourceFolderPath_;
    private File sourceFolder_;
    private File[] sourceImageFiles_;

    public ImageHandler(String folderPath) {
        // TODO handle image files, maybe extract them using LIRE and put results @ console for a start
        if(new File(folderPath).isDirectory()) {
            sourceFolderPath_ = folderPath;
            sourceFolder_ = new File(sourceFolderPath_);
            System.out.println("<<ImageHandler>> image folder is directory");
            File[] imgFiles = sourceFolder_.listFiles(new FilenameFilter() {
                public boolean accept(File dir, String name) {
                    return (name.endsWith(".png") || name.endsWith(".jpeg") || name.endsWith(".jpg"));
                }
            });
            System.out.println("<<ImageHandler>> " + imgFiles.length + " image file(s) found");
            // TODO use LIRE and extract some values
            EdgeHistogram edgeHistogram = new EdgeHistogram();
            for(int i = 0; i < imgFiles.length; i++) {
                try {
                    edgeHistogram.extract(ImageIO.read(imgFiles[i]));
                    System.out.println("<<ImageHandler>> images[" + i + "]: edge histogram feature vectors - " + Arrays.toString(edgeHistogram.getFeatureVector()));
                }
                catch(IOException e) {
                    e.printStackTrace();
                }
            }
        }
        else {
            System.out.println("<<LIRE>> image folder is file (error)");
        }
    }

    public ImageHandler(File sourceImageFolder) {
        // TODO handle image files, maybe extract them using LIRE and put results @ console for a start
        if(sourceImageFolder.isDirectory()) {
            sourceFolder_ = sourceImageFolder;
            System.out.println("<<ImageHandler>> image folder is directory");
            File[] imgFiles = sourceFolder_.listFiles(new FilenameFilter() {
                public boolean accept(File dir, String name) {
                    return (name.endsWith(".png") || name.endsWith(".jpeg") || name.endsWith(".jpg"));
                }
            });
            System.out.println("<<ImageHandler>> " + imgFiles.length + " image file(s) found");
            // TODO use LIRE and extract some values
            EdgeHistogram edgeHistogram = new EdgeHistogram();
            for(int i = 0; i < imgFiles.length; i++) {
                try {
                    edgeHistogram.extract(ImageIO.read(imgFiles[i]));
                    System.out.println("<<ImageHandler>> images[" + i + "]: edge histogram feature vectors - " + Arrays.toString(edgeHistogram.getFeatureVector()));
                }
                catch(IOException e) {
                    e.printStackTrace();
                }
            }
        }
        else {
            System.out.println("<<LIRE>> image folder is file (error)");
        }
    }
}