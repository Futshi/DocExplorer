package DocExplorer.PDF;

import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.graphics.PDXObject;
import org.apache.pdfbox.pdmodel.graphics.image.PDImage;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;

public class PDFHandler {
  private String sourceFolderPath_;
  private File sourceFileFolder_;
  private File[] sourcePdfFiles_;

  // Constructor
  // ...used for test
  public PDFHandler(String sourceFolderPath) {
    if((new File(sourceFolderPath)).isDirectory()) {
      sourceFolderPath_ = sourceFolderPath;
      sourceFileFolder_ = new File(sourceFolderPath_);
      sourcePdfFiles_ = sourceFileFolder_.listFiles(new FilenameFilter() {
        public boolean accept(File dir, String name) {
          return name.endsWith(".pdf");
        }
      });
      System.out.println("<<PDFHandler>> directory " + sourceFolderPath_ + " found, contains " + sourcePdfFiles_.length + " PDF file(s):");
      for(int i = 0; i < sourcePdfFiles_.length; i++) {
        System.out.println("<<PDFHandler>> [" + (i+1) + "] " + sourcePdfFiles_[i].getName());
      }
    }
    else {
      System.out.println("<<PDFHandler>> directory NOT found");
      sourceFileFolder_ = null;
    }
  }

  // Constructor
  // ...once called, automatically extracts images
  public PDFHandler(File sourceFileFolder) {
    if(sourceFileFolder.isDirectory()) {
      sourceFileFolder_ = sourceFileFolder;
      sourcePdfFiles_ = sourceFileFolder_.listFiles(new FilenameFilter() {
        public boolean accept(File dir, String name) {
          return name.endsWith(".pdf");
        }
      });
      System.out.println("<<PDFHandler>> directory " + sourceFileFolder_.getPath() + " found, contains " + sourcePdfFiles_.length + " PDF file(s):");
      for(int i = 0; i < sourcePdfFiles_.length; i++) {
        System.out.println("<<PDFHandler>> [" + (i+1) + "] " + sourcePdfFiles_[i].getName());
      }
      // TODO maybe extract images later...
      extractImages("/home/futshi/Documents/TU/Bachelorarbeit/DocExplorer/resources/img/");
    }
    else {
      System.out.println("<<PDFHandler>> directory NOT found");
      sourceFileFolder_ = null;
    }
  }

  // Function
  // ...extracts images
  public void extractImages(String destinationPath) {
    if(sourcePdfFiles_.length == 0) {
      System.out.println("<<PDFHandler>> can't extract images, no pdf file loaded");
      return;
    }
    try {
      int imageCount = 0;
      for(File pdfFile : sourcePdfFiles_) {
        PDDocument document = PDDocument.load(pdfFile);
        for(PDPage page : document.getPages()) {
          PDResources resources = page.getResources();
          for(COSName cos : resources.getXObjectNames()) {
            // TODO does not cover all "images" (06_vast_bethel.pdf) of an pdf, maybe should use something different than PDXObject
            PDXObject object = resources.getXObject(cos);
            if(object instanceof org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject) {
              File file = new File(destinationPath+ pdfFile.getName().replaceFirst("[.][^.]+$", "") + "_img" + ++imageCount + ".png");
              ImageIO.write(((org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject)object).getImage(), "png", file);
            }
          }
        }
      }
    }
    catch(IOException e) {
      e.printStackTrace();
    }
  }
}
